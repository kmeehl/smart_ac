defmodule SmartAc.Repo.Migrations.AddDeviceIdToEvents do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :device_id, references(:devices)
    end
  end
end
