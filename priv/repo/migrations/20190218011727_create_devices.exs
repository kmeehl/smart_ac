defmodule SmartAc.Repo.Migrations.CreateDevices do
  use Ecto.Migration

  def change do
    create table(:devices) do
      add :serial_number, :string
      add :registered_at, :naive_datetime
      add :firmware_version, :string

      timestamps()
    end
    create unique_index(:devices, [:serial_number])
  end
end
