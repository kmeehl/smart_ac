defmodule SmartAc.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :sensor_co, :float
      add :sensor_temp, :float
      add :sensor_humidity, :float
      add :health, :string
      add :time, :utc_datetime_usec

      timestamps()
    end

  end
end
