use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :smart_ac, SmartAcWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :smart_ac, SmartAc.Repo,
  username: "postgres",
  password: "postgres",
  database: "smart_ac_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
