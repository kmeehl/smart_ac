# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :smart_ac,
  ecto_repos: [SmartAc.Repo]

# Configures the endpoint
config :smart_ac, SmartAcWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "EVaiqJRQHmRak5hRXylnQ/bNBkIrtDF9GAI0Y30R1Q+Q+Mo1d4rwtdk5SYZj0Heo",
  render_errors: [view: SmartAcWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SmartAc.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

# %% Coherence Configuration %%   Don't remove this line
config :coherence,
  user_schema: SmartAc.Coherence.User,
  repo: SmartAc.Repo,
  module: SmartAc,
  web_module: SmartAcWeb,
  router: SmartAcWeb.Router,
  password_hashing_alg: Comeonin.Bcrypt,
  messages_backend: SmartAcWeb.Coherence.Messages,
  registration_permitted_attributes: [
    "email",
    "name",
    "password",
    "current_password",
    "password_confirmation",
    "active"
  ],
  invitation_permitted_attributes: ["name", "email"],
  password_reset_permitted_attributes: [
    "reset_password_token",
    "password",
    "password_confirmation"
  ],
  session_permitted_attributes: ["remember", "email", "password"],
  email_from_name: "Smart AC Admin",
  email_from_email: "admin@acme-smart-ac.com",
  opts: [
    :authenticatable,
    :recoverable,
    :lockable,
    :trackable,
    :unlockable_with_token,
    :invitable,
    :registerable
  ],
  allow_silent_password_recovery_for_unknown_user: true,
  require_current_password: true


# config :coherence, SmartAcWeb.Coherence.Mailer,
#   adapter: Swoosh.Adapters.Sendgrid,
#   api_key: "your api key here"
config :coherence, SmartAcWeb.Coherence.Mailer,
  adapter: Swoosh.Adapters.SMTP,
  relay: "localhost"


# %% End Coherence Configuration %%
