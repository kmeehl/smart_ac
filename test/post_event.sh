#!/bin/bash

SNUM=$1
COUNT=$2

function post () {
  echo "$1 -H Content-Type: application/json -H Authorization: Basic $2 -H api-timestamp: $3"
  curl -X POST 'http://ec2-54-208-30-208.compute-1.amazonaws.com/api/event' -d "$1" -H "Content-Type: application/json" -H "Authorization: Basic $2" -H "api-timestamp: $3"
  echo
}

if [[ -z "$1" ]] || [[ -z "$2" ]] || [[ "$1" -eq "--help" ]]; then
  echo "Usage: $0 <device_serial_number> <event_count> [--batch] [<days_ago>]"
fi

if [[ -n "$3" ]]; then
  echo "POSTing events as a batch"
  PAYLOAD="{\"events\":["
  if [[ "$3" == "--batch" ]]; then
    TIME=`echo $(($(date +%s%N)/1000))`
  else
    TIME=`echo $(($(date -d "-$3 days" +%s%N)/1000))`
  fi
  for i in `seq 1 $COUNT`; do
    CO=`echo $((1 + RANDOM % 8))`
    HUM=`echo $((45 + RANDOM % 8))`
    TEMP=`echo $((26 + RANDOM % 15))`
    TIME=`echo $((1000000 + TIME))`
    PAYLOAD="$PAYLOAD{\"time\":$TIME,\"sensor_co\":$CO,\"sensor_humidity\":$HUM,\"sensor_temp\":$TEMP,\"health\":\"normal\"},"
  done
  PAYLOAD=`echo "$PAYLOAD" |sed 's/,$//'`
  PAYLOAD="$PAYLOAD],\"device\":{\"serial_number\":\"$SNUM\"}}"

  TIMESTAMP=`echo $(($(date +%s%N)/1000000))`
  HMAC=`elixir -e ":crypto.hmac(:sha256, \"secret\", \"$SNUM$TIMESTAMP\") |> Base.encode16 |> IO.puts "`
  #echo $HMAC
  HMAC_AUTH=`elixir -e "\"$SNUM:\" <> \"$HMAC\" |> Base.encode64 |> IO.puts"`
  #echo $HMAC_AUTH

  post "$PAYLOAD" "$HMAC_AUTH" "$TIMESTAMP"
else
  for i in `seq 1 $COUNT`; do
    TIMESTAMP=`echo $(($(date +%s%N)/1000000))`
    HMAC=`elixir -e ":crypto.hmac(:sha256, \"secret\", \"$SNUM$TIMESTAMP\") |> Base.encode16 |> IO.puts "`
    #echo $HMAC
    HMAC_AUTH=`elixir -e "\"$SNUM:\" <> \"$HMAC\" |> Base.encode64 |> IO.puts"`
    #echo $HMAC_AUTH

    CO=`echo $((1 + RANDOM % 8))`
    HUM=`echo $((45 + RANDOM % 8))`
    TEMP=`echo $((30 + RANDOM % 8))`
    TIME=`echo $(($(date +%s%N)/1000))`
    PAYLOAD="{\"event\":{\"time\":$TIME,\"sensor_co\":$CO,\"sensor_humidity\":$HUM,\"sensor_temp\":$TEMP,\"health\":\"normal\"},\"device\":{\"serial_number\":\"$SNUM\"}}"
    post "$PAYLOAD" "$HMAC_AUTH" "$TIMESTAMP"
    sleep 1
  done
fi
