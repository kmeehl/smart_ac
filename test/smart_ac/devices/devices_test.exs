defmodule SmartAc.DevicesTest do
  use SmartAc.DataCase

  alias SmartAc.Devices

  describe "events" do
    alias SmartAc.Devices.Event

    @valid_attrs %{health: "some health", inserted_at: ~N[2010-04-17 14:00:00], sensor_co: 120.5, sensor_humidity: 120.5, sensor_temp: 120.5, timestamp: ~N[2010-04-17 14:00:00], updated_at: ~N[2010-04-17 14:00:00]}
    @update_attrs %{health: "some updated health", inserted_at: ~N[2011-05-18 15:01:01], sensor_co: 456.7, sensor_humidity: 456.7, sensor_temp: 456.7, timestamp: ~N[2011-05-18 15:01:01], updated_at: ~N[2011-05-18 15:01:01]}
    @invalid_attrs %{health: nil, inserted_at: nil, sensor_co: nil, sensor_humidity: nil, sensor_temp: nil, timestamp: nil, updated_at: nil}

    def event_fixture(attrs \\ %{}) do
      {:ok, event} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Devices.create_event()

      event
    end

    test "list_events/0 returns all events" do
      event = event_fixture()
      assert Devices.list_events() == [event]
    end

    test "get_event!/1 returns the event with given id" do
      event = event_fixture()
      assert Devices.get_event!(event.id) == event
    end

    test "create_event/1 with valid data creates a event" do
      assert {:ok, %Event{} = event} = Devices.create_event(@valid_attrs)
      assert event.health == "some health"
      assert event.inserted_at == ~N[2010-04-17 14:00:00]
      assert event.sensor_co == 120.5
      assert event.sensor_humidity == 120.5
      assert event.sensor_temp == 120.5
      assert event.timestamp == ~N[2010-04-17 14:00:00]
      assert event.updated_at == ~N[2010-04-17 14:00:00]
    end

    test "create_event/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Devices.create_event(@invalid_attrs)
    end

    test "update_event/2 with valid data updates the event" do
      event = event_fixture()
      assert {:ok, %Event{} = event} = Devices.update_event(event, @update_attrs)
      assert event.health == "some updated health"
      assert event.inserted_at == ~N[2011-05-18 15:01:01]
      assert event.sensor_co == 456.7
      assert event.sensor_humidity == 456.7
      assert event.sensor_temp == 456.7
      assert event.timestamp == ~N[2011-05-18 15:01:01]
      assert event.updated_at == ~N[2011-05-18 15:01:01]
    end

    test "update_event/2 with invalid data returns error changeset" do
      event = event_fixture()
      assert {:error, %Ecto.Changeset{}} = Devices.update_event(event, @invalid_attrs)
      assert event == Devices.get_event!(event.id)
    end

    test "delete_event/1 deletes the event" do
      event = event_fixture()
      assert {:ok, %Event{}} = Devices.delete_event(event)
      assert_raise Ecto.NoResultsError, fn -> Devices.get_event!(event.id) end
    end

    test "change_event/1 returns a event changeset" do
      event = event_fixture()
      assert %Ecto.Changeset{} = Devices.change_event(event)
    end
  end
end
