defmodule SmartAcWeb.API.EventControllerTest do
  use SmartAcWeb.ConnCase

  alias SmartAc.Devices
  alias SmartAc.Devices.Event

  @create_attrs %{
    health: "some health",
    inserted_at: ~N[2010-04-17 14:00:00],
    sensor_co: 120.5,
    sensor_humidity: 120.5,
    sensor_temp: 120.5,
    timestamp: ~N[2010-04-17 14:00:00],
    updated_at: ~N[2010-04-17 14:00:00]
  }
  @update_attrs %{
    health: "some updated health",
    inserted_at: ~N[2011-05-18 15:01:01],
    sensor_co: 456.7,
    sensor_humidity: 456.7,
    sensor_temp: 456.7,
    timestamp: ~N[2011-05-18 15:01:01],
    updated_at: ~N[2011-05-18 15:01:01]
  }
  @invalid_attrs %{health: nil, inserted_at: nil, sensor_co: nil, sensor_humidity: nil, sensor_temp: nil, timestamp: nil, updated_at: nil}

  def fixture(:event) do
    {:ok, event} = Devices.create_event(@create_attrs)
    event
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all events", %{conn: conn} do
      conn = get(conn, Routes.api_event_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create event" do
    test "renders event when data is valid", %{conn: conn} do
      conn = post(conn, Routes.api_event_path(conn, :create), event: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.api_event_path(conn, :show, id))

      assert %{
               "id" => id,
               "health" => "some health",
               "inserted_at" => "2010-04-17T14:00:00",
               "sensor_co" => 120.5,
               "sensor_humidity" => 120.5,
               "sensor_temp" => 120.5,
               "timestamp" => "2010-04-17T14:00:00",
               "updated_at" => "2010-04-17T14:00:00"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.api_event_path(conn, :create), event: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update event" do
    setup [:create_event]

    test "renders event when data is valid", %{conn: conn, event: %Event{id: id} = event} do
      conn = put(conn, Routes.api_event_path(conn, :update, event), event: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.api_event_path(conn, :show, id))

      assert %{
               "id" => id,
               "health" => "some updated health",
               "inserted_at" => "2011-05-18T15:01:01",
               "sensor_co" => 456.7,
               "sensor_humidity" => 456.7,
               "sensor_temp" => 456.7,
               "timestamp" => "2011-05-18T15:01:01",
               "updated_at" => "2011-05-18T15:01:01"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, event: event} do
      conn = put(conn, Routes.api_event_path(conn, :update, event), event: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete event" do
    setup [:create_event]

    test "deletes chosen event", %{conn: conn, event: event} do
      conn = delete(conn, Routes.api_event_path(conn, :delete, event))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.api_event_path(conn, :show, event))
      end
    end
  end

  defp create_event(_) do
    event = fixture(:event)
    {:ok, event: event}
  end
end
