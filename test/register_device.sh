#!/bin/bash

SNUM=$1

TIMESTAMP=`echo $(($(date +%s%N)/1000000))`
HMAC=`elixir -e ":crypto.hmac(:sha256, \"secret\", \"$SNUM$TIMESTAMP\") |> Base.encode16 |> IO.puts "`
echo $HMAC
HMAC_AUTH=`elixir -e "\"$SNUM:\" <> \"$HMAC\" |> Base.encode64 |> IO.puts"`
echo $HMAC_AUTH

PAYLOAD="{\"device\":{\"serial_number\":\"$1\",\"firmware_version\":"1.0"}}"
curl -X POST 'http://ec2-54-208-30-208.compute-1.amazonaws.com/api/device' -d "$PAYLOAD" -H "Content-Type: application/json" -H "Authorization: Basic $HMAC_AUTH" -H "api-timestamp: $TIMESTAMP" 
