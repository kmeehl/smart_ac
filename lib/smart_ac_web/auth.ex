defmodule SmartAcWeb.Auth do

  defp inspect_conn(conn, term) do
    IO.puts "#{term} - Halted: #{conn.halted}"
    conn
  end
  def api_authorize(conn, username, password) do
    conn = conn
    |> validate_timestamp()
    |> inspect_conn("time")
    |> validate_username(username)
    |> inspect_conn("username")
    |> validate_password(password)
    |> inspect_conn("password")
  end

  defp validate_timestamp(%Plug.Conn{halted: true} = conn), do: conn

  defp validate_timestamp(conn) do
    with timestamp <- timestamp_req_header(conn),
      {:ok, timestamp} <- DateTime.from_unix(timestamp, :milliseconds),
      diff <- DateTime.diff(timestamp, DateTime.utc_now()),
      diff <- abs(diff),
      true <- diff < 10 # within 20 seconds of server time
    do
      conn
    else
      _ -> Plug.Conn.halt(conn)
    end
  end

  defp validate_username(%Plug.Conn{halted: true} = conn, _), do: conn

  defp validate_username(conn, username) do
    device_id = Map.get(conn.params,"device", %{})
    |> Map.get("serial_number")
    IO.puts device_id
    IO.puts username
    if !is_nil(device_id) && String.equivalent?(username, device_id) do
      conn
    else
      Plug.Conn.halt(conn)
    end
  end

  defp validate_password(%Plug.Conn{halted: true} = conn, _), do: conn

  defp validate_password(conn, password) do
    with timestamp <- timestamp_req_header(conn),
      device <- Map.get(conn.params, "device", %{}),
      device_id <- Map.get(device, "serial_number"),
      _ <- IO.inspect(password),
      hmac <- :crypto.hmac(:sha256, "secret", device_id <> to_string(timestamp)),
      hmac_token <-  Base.encode16(hmac),
      _ <- IO.inspect(hmac_token),
      true <- String.equivalent?(password, hmac_token)
    do
      conn
    else
      _ -> Plug.Conn.halt(conn)
    end
  end

  defp timestamp_req_header(conn) do
    with [timestamp] <- Plug.Conn.get_req_header(conn, "api-timestamp"),
      {timestamp, _} <- Integer.parse(timestamp)
    do
      timestamp
    else
      _ -> :error
    end
  end
end
