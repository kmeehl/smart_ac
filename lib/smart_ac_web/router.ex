defmodule SmartAcWeb.Router do
  use SmartAcWeb, :router
  use Coherence.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug(Coherence.Authentication.Session)
  end

  pipeline :protected do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug(Coherence.Authentication.Session, protected: true)
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug BasicAuth, callback: &SmartAcWeb.Auth.api_authorize/3
    # plug :protect_from_forgery
    plug :put_secure_browser_headers
    # plug SmartAc.ApiAuth
  end

  scope "/", SmartAcWeb do
    pipe_through(:browser)
    get "/", PageController, :index
    resources "/devices", DeviceController, only: [:index, :show]
  end

  scope "/api", SmartAcWeb do
    pipe_through(:api)
  #   # resources "/events", EventsController
    post "/event", API.EventController, :create
    post "/device", API.DeviceController, :create
  end

  scope "/" do
    pipe_through(:browser)
    # resources "/sessions", Coherence.SessionController, only: [:create, :new]
    # resources "/registrations", Coherence.RegistrationController, only: [:new, :create]
    coherence_routes()
  end

  scope "/" do
    pipe_through(:protected)
    # resources "/sessions", Coherence.SessionController, only: [:delete]
    get "/sessions/delete", Coherence.SessionController, :delete
    # resources "/registrations", Coherence.RegistrationController, only: [:show, :edit, :update, :delete]
    # resources "/passwords", Coherence.PasswordController
    # resources "/unlocks", Coherence.UnlockController
    # resources "/invitations", Coherence.InvitationController, only: [:new, :create, :delete]
    coherence_routes(:protected)
    resources "/registrations", SmartAcWeb.Coherence.RegistrationController, only: [:edit, :update]
  end
end
