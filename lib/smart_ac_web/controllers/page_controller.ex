defmodule SmartAcWeb.PageController do
  use SmartAcWeb, :controller

  def index(conn, _params) do
    # render(conn, "index.html")
    if Coherence.logged_in?(conn) do
      redirect(conn, to: "/devices")
    else
      redirect(conn, to: Routes.session_path(conn, :new))
    end

  end
end
