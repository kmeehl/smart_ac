defmodule SmartAcWeb.API.EventController do
  use SmartAcWeb, :controller

  alias SmartAc.Devices
  alias SmartAc.Devices.Event
  alias SmartAcWeb.Router.Helpers, as: Routes

  action_fallback SmartAcWeb.FallbackController

  # def index(conn, _params) do
  #   events = Devices.list_events()
  #   render(conn, "index.json", events: events)
  # end

  def create(conn, %{"event" => _event_attrs, "device" => _device_attrs} = attrs) do
    with {:ok, %Event{} = event} <- Devices.create_event(attrs) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.event_path(conn, :create))
      |> render("show.json", event: event)
    else
      {:error, changeset} ->
      conn
      |> put_resp_header("location", Routes.event_path(conn, :create))
      |> render("error.json", changeset)
    end
  end

  def create(conn, %{"events" => events, "device" => _device_attrs} = attrs) do
    with {:ok, events} <- Devices.create_events(attrs) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.event_path(conn, :create))
      |> render("show.json", events: events)
    else
      {:error, [changeset | _rest]} ->
      conn
      |> put_resp_header("location", Routes.event_path(conn, :create))
      |> render("error.json", changeset)
    end
  end

  # def show(conn, %{"id" => id}) do
  #   event = Devices.get_event!(id)
  #   render(conn, "show.json", event: event)
  # end
  #
  # def update(conn, %{"id" => id, "event" => event_params}) do
  #   event = Devices.get_event!(id)
  #
  #   with {:ok, %Event{} = event} <- Devices.update_event(event, event_params) do
  #     render(conn, "show.json", event: event)
  #   end
  # end
  #
  # def delete(conn, %{"id" => id}) do
  #   event = Devices.get_event!(id)
  #
  #   with {:ok, %Event{}} <- Devices.delete_event(event) do
  #     send_resp(conn, :no_content, "")
  #   end
  # end
end
