defmodule SmartAcWeb.API.DeviceController do
  use SmartAcWeb, :controller

  alias SmartAc.Device
  alias SmartAcWeb.Router.Helpers, as: Routes

  action_fallback SmartAcWeb.FallbackController

  def create(conn, %{"device" => device_attrs}) do
    with {:ok, %Device{} = device} <- Device.create_device(device_attrs) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.device_path(conn, :create))
      |> render("show.json", device: device)
    end
  end
end
