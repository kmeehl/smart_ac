defmodule SmartAcWeb.Coherence.RegistrationController do
  @moduledoc """
  Handle account registration actions.

  Actions:

  * new - render the register form
  * create - create a new user account
  * edit - edit the user account
  * update - update the user account
  * delete - delete the user account
  """
  use CoherenceWeb, :controller
  use Coherence.RegistrationControllerBase, schemas: SmartAc.Coherence.Schemas

  plug(Coherence.RequireLogin when action in ~w(show edit update delete)a)
  plug(Coherence.ValidateOption, :registerable)
  plug(:scrub_params, "registration" when action in [:create, :update])

  plug(:layout_view, view: Coherence.RegistrationView, caller: __MODULE__)
  plug(:redirect_logged_in when action in [:new, :create])

  def edit(conn, %{"id" => id}) do
    user = Coherence.Schemas.get_user(id)
    render_edit(conn, user)
  end

  def update(conn, %{"registration" => user_params, "id" => registration_id} = params) do
    user_schema = Config.user_schema()
    initial_user = Coherence.Schemas.get_user(registration_id)

    :registration
    |> Controller.changeset(
      user_schema,
      initial_user,
      Controller.permit(
        user_params,
        Config.registration_permitted_attributes() ||
          Schema.permitted_attributes_default(:registration)
      )
    )
    |> @schemas.update
    |> case do
      {:ok, user} ->
        if Config.get(:confirm_email_updates) &&
             user.unconfirmed_email != initial_user.unconfirmed_email do
          send_confirmation(conn, user, user_schema)
        end

        unless user.active == true do
          Coherence.CredentialStore.Server.delete_user_logins(user)
          if user.id == Coherence.current_user(conn).id do
            require IEx;IEx.pry
            Coherence.Authentication.Session.delete_login(conn)
            # Coherence.Authentication.Utils.delete_token_session(conn)
            # Coherence.Authentication.Utils.delete_user_token(conn)
          end
        end

        Config.auth_module()
        |> apply(Config.update_login(), [conn, user, [id_key: Config.schema_key()]])
        |> respond_with(
          :registration_update_success,
          %{
            user: user,
                params: params,
                info: Messages.backend().account_updated_successfully()
              }
            )

          {:error, changeset} ->
            respond_with(conn, :registration_update_error, %{
              user: initial_user,
              changeset: changeset
            })
        end
  end


  defp render_edit(conn, user) do
    changeset = Controller.changeset(:registration, user.__struct__, user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end
end
