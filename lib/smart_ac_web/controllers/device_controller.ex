defmodule SmartAcWeb.DeviceController do
  use SmartAcWeb, :controller

  alias SmartAc.{Device, Repo}
  plug(Coherence.RequireLogin when action in ~w(index show edit update delete)a)

  def index(conn, _params) do
    devices = Repo.all(Device) |> Repo.preload(:events)
    render(conn, "index.html", devices: devices)
  end

  def show(conn, %{"id" => id} = params) do
    range = Map.get(params, "range", "year")
    range_limit = case range do
      "today" ->
        Timex.shift(Timex.now, days: -1)
      "week" ->
        Timex.shift(Timex.now, days: -7)
      "month" ->
        Timex.shift(Timex.now, months: -1)
      "year" ->
        Timex.shift(Timex.now, years: -1)
    end

    device = Repo.get(Device, id) |> Repo.preload(:events)
    events = Enum.filter(device.events, fn event ->
      Timex.compare(range_limit, event.time) < 0
    end)
    |> Enum.sort(fn a,b -> Timex.compare(a.time, b.time) < 0 end)
    render(conn, "show.html", device: device, events: events, range: range)
  end

  # def edit(conn, %{"id" => id}) do
  #   device = WebDevices.get_device!(id)
  #   changeset = WebDevices.change_device(device)
  #   render(conn, "edit.html", device: device, changeset: changeset)
  # end
  #
  # def update(conn, %{"id" => id, "device" => device_params}) do
  #   device = WebDevices.get_device!(id)
  #
  #   case WebDevices.update_device(device, device_params) do
  #     {:ok, device} ->
  #       conn
  #       |> put_flash(:info, "Device updated successfully.")
  #       |> redirect(to: Routes.device_path(conn, :show, device))
  #
  #     {:error, %Ecto.Changeset{} = changeset} ->
  #       render(conn, "edit.html", device: device, changeset: changeset)
  #   end
  # end
  #
  # def delete(conn, %{"id" => id}) do
  #   device = WebDevices.get_device!(id)
  #   {:ok, _device} = WebDevices.delete_device(device)
  #
  #   conn
  #   |> put_flash(:info, "Device deleted successfully.")
  #   |> redirect(to: Routes.device_path(conn, :index))
  # end
end
