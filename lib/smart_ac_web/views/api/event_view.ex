defmodule SmartAcWeb.API.EventView do
  use SmartAcWeb, :view
  alias SmartAcWeb.API.EventView

  def render("show.json", %{events: events}) do
    %{events: render_many(events, EventView, "event.json")}
  end

  def render("show.json", %{event: event}) do
    %{event: render_one(event, EventView, "event.json")}
  end

  def render("error.json", %Ecto.Changeset{} = changeset) do
    %{event: %{errors: Ecto.Changeset.traverse_errors(changeset, fn {msg,_} -> to_string(msg) end)}}
  end

  def render("errors.json", changesets) do
    %{events: Enum.map(changesets, fn changeset ->
      %{errors: Ecto.Changeset.traverse_errors(changeset, fn {msg,_} -> to_string(msg) end)}
    end)}
  end

  def render("event.json", %{event: event}) do
    %{id: event.id,
      sensor_co: event.sensor_co,
      sensor_temp: event.sensor_temp,
      sensor_humidity: event.sensor_humidity,
      health: event.health,
      time: event.time}
  end
end
