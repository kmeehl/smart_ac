defmodule SmartAcWeb.API.DeviceView do
  use SmartAcWeb, :view
  alias SmartAcWeb.API.DeviceView

  def render("show.json", %{device: device}) do
    %{device: render_one(device, DeviceView, "device.json")}
  end

  def render("device.json", %{device: device}) do
    %{ serial_number: device.serial_number,
       firmware_version: device.firmware_version,
       registered_at: device.registered_at}
  end
end
