defmodule SmartAc.Device do
  use Ecto.Schema
  import Ecto.Changeset
  alias SmartAc.Repo


  schema "devices" do
    field :firmware_version, :string
    field :registered_at, :naive_datetime
    field :serial_number, :string
    has_many :events, SmartAc.Devices.Event, on_delete: :delete_all

    timestamps()
  end

  def create_device(attrs) do
    {_, attrs} = Map.get_and_update(attrs, "firmware_version", fn v -> {v, to_string(v)} end)

    attrs = Map.merge(attrs, %{"registered_at" => DateTime.utc_now(),
                               "firmware_version" => Map.get(attrs, "firmware_version") |> to_string()})
    %SmartAc.Device{}
    |> changeset(attrs)
    |> Repo.insert()
  end

  @doc false
  def changeset(device, attrs) do
    device
    |> cast(attrs, [:serial_number, :registered_at, :firmware_version])
    |> validate_required([:serial_number, :registered_at, :firmware_version])
  end
end
