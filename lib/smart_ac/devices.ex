defmodule SmartAc.Devices do
  @moduledoc """
  The Devices context.
  """

  import Ecto.Query, warn: false
  alias SmartAc.{Repo, Device}
  alias Ecto.{Multi, Changeset}

  alias SmartAc.Devices.Event

  @doc """
  Returns the list of events.

  ## Examples

      iex> list_events()
      [%Event{}, ...]

  """
  def list_events do
    Repo.all(Event)
  end

  @doc """
  Gets a single event.

  Raises `Ecto.NoResultsError` if the Event does not exist.

  ## Examples

      iex> get_event!(123)
      %Event{}

      iex> get_event!(456)
      ** (Ecto.NoResultsError)

  """
  def get_event!(id), do: Repo.get!(Event, id)

  @doc """
  Creates a event.

  ## Examples

      iex> event = %{"event" => %{"time" => 1550361422194000,"sensor_co" => "5","sensor_humidity" => "45","sensor_temp" => "33","health" => "normal"},"device" => %{"serial_number" => "ABC"}}
      %{
        "device" => %{"serial_number" => "ABC"},
        "event" => %{
          "health" => "normal",
          "sensor_co" => "5",
          "sensor_humidity" => "45",
          "sensor_temp" => "33",
          "time" => 1550361422194000
        }
      }
      iex> create_event(event)
      {:ok,
       %SmartAc.Devices.Event{
         __meta__: #Ecto.Schema.Metadata<:loaded, "events">,
         device: %SmartAc.Device{
           __meta__: #Ecto.Schema.Metadata<:loaded, "devices">,
           events: [
             %SmartAc.Devices.Event{
               __meta__: #Ecto.Schema.Metadata<:loaded, "events">,
               device: #Ecto.Association.NotLoaded<association :device is not loaded>,
               device_id: 2,
               health: "normal",
               id: 50,
               inserted_at: ~N[2019-02-18 03:15:44],
               sensor_co: 2.0,
               sensor_humidity: 45.0,
               sensor_temp: 32.0,
               time: #DateTime<2019-02-16 23:57:02.194000Z>,
               updated_at: ~N[2019-02-18 03:15:44]
             }],
           firmware_version: "1.0",
           id: 2,
           inserted_at: ~N[2019-02-18 02:42:28],
           registered_at: ~N[2019-02-18 02:42:28],
           serial_number: "ABC",
           updated_at: ~N[2019-02-18 02:42:28]
         },
         device_id: 2,
         health: "normal",
         id: 51,
         inserted_at: ~N[2019-02-18 15:19:09],
         sensor_co: 5.0,
         sensor_humidity: 45.0,
         sensor_temp: 33.0,
         time: #DateTime<2019-02-16 23:57:02.194000Z>,
         updated_at: ~N[2019-02-18 15:19:09]
       }}


      iex> create_event(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_event(attrs \\ %{}) do
    with device <- get_device(attrs)
    do
      %Event{}
      |> Event.changeset(device, attrs["event"])
      |> case do
        %Changeset{valid?: false} = changeset ->
          {:error, changeset}
        %Changeset{} = changeset ->
          Repo.insert(changeset)
      end
    end
  end

  @doc """
  Creates a list of events owned by `device` event.

  ## Examples

      iex> create_event(%{field: value})
      {:ok, %Event{}}

      iex> create_event(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_events(%{"events" => events_attrs, "device" => _device_attrs} = attrs \\ %{}) when is_list(events_attrs)  do
    with true <- length(events_attrs) <= 500,
         device <- get_device(attrs)
    do
      # require IEx;IEx.pry
      {_, result} = Repo.transaction(fn ->
        Enum.map(events_attrs, fn event_attrs -> Event.changeset(%Event{}, device, event_attrs) end)
        |> Enum.reduce_while({:ok, []}, fn event, {:ok, results} ->
             case Repo.insert(event) do
               {:ok, result} -> {:cont, {:ok, [result | results]}}
               {:error, result} -> {:halt, {:error, [result | results]}}
             end
           end)
      end)
      result
    else
      _ -> {:error, [Ecto.Changeset.change(%Event{}) |> Ecto.Changeset.add_error(:base, "invalid")]}
    end
  end

  defp get_device(%{"device" => _device_attrs} = attrs) do
    device = Repo.get_by(Device, serial_number: attrs["device"]["serial_number"])
    |> Repo.preload([:events])
    case device do
      nil -> {:error}
      _ -> device
    end
  end

  @doc """
  Updates a event.

  ## Examples

      iex> update_event(event, %{field: new_value})
      {:ok, %Event{}}

      iex> update_event(event, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_event(%Event{} = event, attrs) do
    event
    |> Event.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Event.

  ## Examples

      iex> delete_event(event)
      {:ok, %Event{}}

      iex> delete_event(event)
      {:error, %Ecto.Changeset{}}

  """
  def delete_event(%Event{} = event) do
    Repo.delete(event)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking event changes.

  ## Examples

      iex> change_event(event)
      %Ecto.Changeset{source: %Event{}}

  """
  def change_event(%Event{} = event) do
    Event.changeset(event, %{})
  end
end
