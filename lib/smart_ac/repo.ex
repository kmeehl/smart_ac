defmodule SmartAc.Repo do
  use Ecto.Repo,
    otp_app: :smart_ac,
    adapter: Ecto.Adapters.Postgres
end
