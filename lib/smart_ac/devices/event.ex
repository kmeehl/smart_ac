defmodule SmartAc.Devices.Event do
  use Ecto.Schema
  import Ecto.Changeset
  alias SmartAc.Device


  schema "events" do
    field :health, :string
    field :sensor_co, :float
    field :sensor_humidity, :float
    field :sensor_temp, :float
    field :time, :utc_datetime_usec
    belongs_to :device, Device

    timestamps()
  end

  @doc false
  def changeset(event, %Device{} = device, attrs) do
    changeset(event, attrs)
    |> Ecto.Changeset.put_assoc(:device, device)
  end

  def changeset(event, _device, attrs), do: changeset(event, attrs) |> Ecto.Changeset.add_error(:base, "invalid")

  @doc false
  def changeset(event, attrs) do
    event
    |> cast(attrs, [:sensor_co, :sensor_temp, :sensor_humidity, :health, :device_id])
    |> cast_timestamp(attrs, :time)
    |> validate_required([:sensor_co, :sensor_temp, :sensor_humidity, :health, :time])
    |> validate_inclusion(:health, ~w(normal needs_service needs_new_filter gas_leak))
    |> validate_length(:health, max: 149)
  end

  defp cast_timestamp(%SmartAc.Devices.Event{} = event, params, field) when is_atom(field) do
    cast_timestamp(Ecto.Changeset.change(event), params, field)
  end

  defp cast_timestamp(%Ecto.Changeset{} = changeset, params, field) when is_atom(field) do
    params
    |> Map.get(to_string(field), -1)
    |> DateTime.from_unix(:microseconds)
    |> case do
      {:ok, datetime} -> put_change(changeset, field, datetime)
      {:error, reason} -> Ecto.Changeset.add_error(changeset, field, to_string(reason))
    end
  end

  def pry(c) do
    require IEx;IEx.pry
    c
  end
end
